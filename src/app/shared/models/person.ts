export class Person {

    private name: string;
    private _age: number;

    constructor(name:string, age:number){
        this.name=name;
        this.age=age;
    }
    public get age(): number {
        return this._age;
    }
    public set age(value: number) {
        this._age = value;
    }


}
