import { Injectable } from '@angular/core';
import { Person } from './shared/models/person';

@Injectable({
  providedIn: 'root'
})
export class PersonService {
  public getPersons():Person[]{

    return [ 
      new Person('usmanium', 100),
      new Person('lucas', 100),
      new Person('vivien', 100)


    ];
  }

  

  constructor() { }
   
}
