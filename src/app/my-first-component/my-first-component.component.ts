import { Component, OnInit, Injectable } from '@angular/core';
import { PersonService } from '../person.service';
import { Person } from '../shared/models/person';

@Component({
  selector: 'app-my-first-component',
  templateUrl: './my-first-component.component.html',
  styleUrls: ['./my-first-component.component.css']
})



export class MyFirstComponentComponent implements OnInit {
  public persons: Person[];

  
  

 constructor(public personService:PersonService) { }

  ngOnInit(): void {
    this.persons = this.personService.getPersons()
  }

}
