import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyFirstComponentComponent } from './my-first-component/my-first-component.component';


const routes: Routes = [{ path: 'crisis-center', component: MyFirstComponentComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
